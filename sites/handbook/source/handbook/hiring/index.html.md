---
layout: handbook-page-toc
title: "Hiring & Talent Acquisition Handbook"
description: "Landing page for many of the handbook pages the talent acquisition team at GitLab uses."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}


# Introduction 
At the core of our team’s vision, mission, and strategy is our ability to impact GitLab’s overarching [mission](https://about.gitlab.com/company/mission/): to make it so that **everyone can contribute**. When **everyone can contribute**, users become contributors and we greatly increase the rate of innovation. 

As a Talent Acquisition team, we have an outsized impact on GitLab’s ability to make this mission a reality, by connecting top talent to profound careers from wherever they are in a truly distributed, remote workforce. 

### Talent Acquisition Vision Statement
To create globally inclusive access to opportunities so that **everyone can contribute**.

### Talent Acquisition Mission Statement
It is the Talent Acquisition Team’s mission to predictably build distributed, representative teams that enable our vision of creating globally inclusive access to opportunities so that **everyone can contribute**. 

### Our Guiding Principles
As we set out over the next decade to achieve this vision, we will continue to rely on core guiding principles to define how we build toward the future. 
  1. **Experience**: Stakeholder experience is central to all that we do. We are not purely a service provider but a partner and advisor in creating a best-in-class experience while building GitLab as a company. The Talent Acquisition team looks after building an experience for our customers, stakeholders, and partners that stands true to our [CREDIT](https://about.gitlab.com/handbook/values/) values and provides a level of care we are proud of - no matter the outcome. 
  1. **Inclusivity**: Applying best practices in the craft of Talent Acquisition has an outsized impact on our ability to build a representative workforce here at GitLab. By embedding inclusivity as a guiding principle, we design fair and equitable processes into the fabric of all that we do, rather than retroactively adding parameters to solve systemic challenges in the societies we exist in   
  1. **Predictability**: Our ability to have the right people, in the right jobs, at the right time is imperative to our ability to execute our commitments and plans as an organization. In order to achieve that, our team must design with an eye for accuracy and forecastability in any program, process or experience we enable. 

### Our Objectives and Roadmap
We strive to be as transparent as possible, but these sections are only available for our GitLab team members.
[FY24 TA Strategy](https://docs.google.com/document/d/1Q9ui_BZLRPBAYpRtdtpHVSvGzW3g7JtVYkGj_KqwZBA/edit#heading=h.riu5wc8sjmum)



# Talent Acquisition Pages and Processes


## Team Process Pages

- [TA Key Performance Indicators](https://internal-handbook.gitlab.io/handbook/people-group/talent-acquisition/key-performance-indicators/) Note: this page is currently in our internal handbook as they are a work in progress.
- [Meeting Cadence](/handbook/hiring/meetings/)
- [Talent Acquisition Alignment](/handbook/hiring/recruiting-alignment/)
- [Diversity, Inclusion & Belonging Talent Acquisition Initiatives](/company/culture/inclusion/talent-acquisition-initiatives/)
- [Triad Process](/handbook/hiring/talent-acquisition-framework/triadprocess/)

<details>
<summary markdown="span">Shared Definitions</summary>

* **Job:** A job refers to the job title (ex: Customer Support Specialist). This will also be what appears on external job boards. In the case there are multiple positions open that are the same, and we only want to list once, we can have multiple 'openings' (see next section) opened within one 'Job'. Each job will have a unique identifier called a Requisition ID (example- 1001).

* **Opening:** A job can have multiple openings attached to it (ex: you are hiring 3 Customer Support Specialists. You would then have 1 ‘Job’ and 3 ‘openings’ against that job). A job can have multiple openings against it, but an opening can not be associated with multiple jobs. Each opening will have a unique identifier called an Opening ID (example- 1001-1, 1001-2, 1001-3).

* **GHPiD:** GHP ID is the link between Adaptive (what we use to track our operating plan) and Greenhouse (our ATS). A GHP ID has a one to one relationship with an Opening ID. It is the key interlock between our hiring plans and our Talent Acquisition activity. This is a custom field in Greenhouse.

</details>


## Candidate Handbook Pages
Please find pages for potential and active applicants below.

- [Candidate Handbook Page](/handbook/hiring/candidate/faq/)
- [Talent Acquisition Privacy Policy](/handbook/hiring/candidate/faq/recruitment-privacy-policy/)


## Interviewer Processes

- [Interviewer Prep Requirements](/handbook/hiring/interviewing/)
- [Conducting a GitLab Interview](/handbook/hiring/conducting-a-gitlab-interview/)
- [Greenhouse for Interviewers](/handbook/hiring/greenhouse/#for-all-interviewers/)


## Hiring Manager Processes

- [Hiring Manager Processes](/handbook/hiring/talent-acquisition-framework/hiring-manager/)
- [Conducting a GitLab Interview](/handbook/hiring/conducting-a-gitlab-interview/)
- [Greenhouse for Hiring Managers](/handbook/hiring/greenhouse/#for-hiring-managers)

## Candidate Experience Specialist Processes

<details>
<summary markdown="span">Greenhouse integrations you'll need</summary>
  * [Prelude](/handbook/hiring/prelude/): To gain CES-level access to Prelude, ask your manager to message the Support team at Prelude.<br>
  * [Guide](https://support.greenhouse.io/hc/en-us/articles/360052205072-Guide-integration): Check with your manager if you do not have higher level access to navigate inside of Prelude.<br>
  * [DocuSign](https://support.greenhouse.io/hc/en-us/articles/205633569-DocuSign-integration)

</details>

- [Candidate Experience Specialist Responsibilities](/handbook/hiring/talent-acquisition-framework/coordinator/)
- [Prelude](/handbook/hiring/prelude)
- [How to Complete a Contract - CES Process](/handbook/hiring/talent-acquisition-framework/ces-contract-processes/)



## Recruiters and Sourcers


#### Opening a job


<details>
<summary markdown="span">Get your headcount assignments</summary>
* [How to open headcount on R&D](/handbook/engineering/#hiring-practices)
* [How to open headcount on Sales and G&A](/handbook/hiring/talent-acquisition-framework/req-creation/#creation-and-approval)
</details>

<details>
<summary markdown="span">Open a req in Greenhouse</summary>
* [Instructions here](/handbook/hiring/talent-acquisition-framework/req-creation/#opening-vacancies-in-greenhouse)<br>
* [Open a kickoff session](/handbook/hiring/talent-acquisition-framework/req-overview/#step-3-complete-kick-off-session-agree-on-priority-level--complete-a-sourcing-session)
</details>

<details>
<summary markdown="span">Evergreen requisitions</summary>

  * [Creating an evergreen req](/handbook/hiring/talent-acquisition-framework/evergreen-requisitions/)<br>
  * [Evergreen req guide](/handbook/hiring/talent-acquisition-framework/evergreen-requisitions/)<br>
  * An Evergreen Job is a requisition that is 'always open'. More specifically, it is used when we have at least 3 openings for a particular job repeated each quarter. There is then one Evergreen job posted for internal, external and passive candidates. It is important that no candidate is hired to an Evergreen job, and instead is moved to an approved opening (aka an opening with a single corresponding GHPiD). TA Leadership will open EVG roles at the start of a fiscal year, with a quarterly review cadence, by leveraging the hiring plan and attrition assumptions. See the pages above for more information.<br>
  + <details><summary markdown="span">Tips and Tricks</summary>
   
     * Know your department before you open your req. If you need to change the department name later, the approval chain will not automatically update. If this happens to you, please contact Enablement.
     * We do not re-open jobs after they have been closed. This is because permissions and approvers may have changed in the time since the job was first closed.
</details>

<details>
<summary markdown="span">Post a job in Greenhouse</summary>

   * [Post an **internal** job](/handbook/hiring/talent-acquisition-framework/req-creation/#opening-vacancies-in-greenhouse-dri-recruiter)<br>
   * [Post an **external** job](/handbook/hiring/talent-acquisition-framework/req-creation/#publish-the-job-to-the-careers-page--review-greenhouse-configuration)<br>
   * [Using Rules](/handbook/hiring/talent-acquisition-framework/req-creation/#using-job-posting-rules-in-greenhouse/)
   * [Auto-tags](/handbook/hiring/greenhouse/#auto-tags)
   + <details><summary markdown="span">Tips and Tricks</summary>
     * Salary transparency: In select US states, GitLab discloses salaries in job descriptions in accordance with local laws. Total Rewards will add this information to the job’s approvals page. When you create your _external_ job post, utilize Pay Transparency Rules and select Hiring in the USA. If the job will not be posted in the US, select N/A.<br>
     * EEOC questions should be checked off in the US only<br>
     * [Click here](/handbook/hiring/talent-acquisition-framework/req-creation/#choosing-the-correct-location-for-your-published-job) for more information on selecting the correct location for your job. This will help ensure that your LinkedIn listings are posted in the correct countries.<br>
     * Our jobs are now set up to automatically go to Indeed and Glassdoor and be posted as remote roles on both sites. Recruiters should unselect or leave unselected the option in Greenhouse to "publish to free job boards" as this requires us to input city, state, and country data that overrides the remote job listing. There is an automation in place to automatically send jobs to Indeed and Glassdoor.
  </details>



<details><summary markdown="span">Req access and permissions</summary>

  * As a member of the Talent Acquisition team, you have the ability to add team members to view your job with differing layers of access. By default, all team members can access the interview kit & scorecard of someone they’re interviewing and this access does not need to be granted. For team members who require additional access, such as a Hiring Manager or their EBA, you will need to grant that access where appropriate.<br>
  * As you are deciding what access level someone should have, default to the setting that allows the most confidentiality for a candidate. If a team member does not need access to scorecards, for example, we should be choosing an access level that does not allow them to see them. A job admin (someone with higher level permissions) typically should not be at the same level or a direct report of a job they have access to. Enablment is in the process of creating a table with specific information about each level, which we will link to here when it is complete.<br>
  * To add or remove access in a job, go to ‘job setup’ and click ‘hiring team’. Add or remove access under “Who can see this job?”. Unfortunately, there is no way to bulk add or remove access. You can also add and remove access when creating your job from a template. If you are searching for a team member and cannot find their name, ensure that you do not have filters activated. Do not add access to templates, because this could create problems in the future.
  </details>


<details><summary markdown="span">Set up scorecards and interview plans</summary>

  * There are two elements of a scorecard: the Scorecard section and the Interview Plan section. Both are accessible through Job Setup.
  * [**Scorecard**](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/req-creation/#update-the-scorecard) Depending on your department, you may have a lot of your scorecard options pre-filled. R&D scorecards are always set up for you, and may only require small tweaks depending on technical language or job grade.
  * [**Interview Plan**](https://support.greenhouse.io/hc/en-us/articles/115002276366-Add-an-interview-question-to-an-interview-kit)
  + <details><summary markdown="span">Tips and Tricks</summary>

    * Name your interviews in a way that will help your Candidate Experience Specialist partner schedule your interview. We recommend ‘Topic -  Interviewer - Time’ i.e. ‘Values Interview - Beyonce Knowles - 45 min’. If you are pulling from a large interviewer pool, you can discuss with your Candidate Experience Specialist partner if it makes sense to create a Prelude interviewer pool that will help with scheduling.
    * Once you have set up the stages and interview names, add the specific interview questions asked into the “Interview Questions” section of the Interview Plan.
    * If you need to make bulk changes to a large number of jobs’ scorecards at once, Enablement has the ability to make bulk changes. Please contact them for assistance.
    * Recruiters don’t need to be the only ones adding interview questions to the interview plan. Invite your Hiring Manager to complete this task alongside you.
    </details>
</details>

#### Interview Stages

<details>
<summary markdown="span">Screening Stage</summary>

  * [Screening Stage guidelines](/handbook/hiring/talent-acquisition-framework/req-overview/#screening)
  * All phone screens are scheduled through Calendly by the Recruiter. [Click here](https://support.greenhouse.io/hc/en-us/articles/360029359472-Enable-Calendly-integration) for instructions on how to set up the Calendly-Greenhouse integration. All Recruiters have a Calendly subscription through the Talent Acquisition plan, so please check with Enablement if you don’t have one.
  * Even if you know you won’t hire the candidate, recruiters should always put scorecards into Greenhouse within 24 hours of a screen. Should that candidate apply again, other recruiters will want to review your notes.
  * To maintain privacy and confidentiality for the candidate, all mention of salary or personal information should be recorded as private notes. Salary information should never be written in a candidate’s scorecard. If a candidate discloses information about being part of a protected class, such as their ethnicity, current pregnancy, or sexual orientation, refrain from writing any notes referencing this.
  * Click [here](/handbook/hiring/talent-acquisition-framework/req-overview/#screening) to find where some of your potentially confidential information about a candidate can be placed.
</details>

- [Assessments](/handbook/hiring/talent-acquisition-framework/req-creation/#update-the-interview-plan)
- [Team Interview](/handbook/hiring/talent-acquisition-framework/req-overview/#team-interview)
<details>
<summary markdown="span">Scheduling tools in Greenhouse</summary>

  * Candidate Experience Specialists use [Prelude](/handbook/hiring/prelude/#prelude) to schedule.
  * Interview confirmations are sent through [Guide](/handbook/hiring/guide/#guide). Because of this, confirmations have calendar invites that can be downloaded rather than being sent through. For this reason, Recruiters are not able to be added to candidate interview invites.
</details>

<details>
<summary markdown="span">Greenhouse for internal candidates</summary>

  * There are a few elements of Greenhouse that are different for internal candidates. Please note that internal candidates refer to current, full time team members. Interns converting to full time, contractors, or rehires are not considered internal candidates.
  * Interviews will always be scheduled as “private” events to ensure the candidate’s privacy. Interviews will show up as “busy” events on candidate and interviewers calendars rather than showing as interviews.
  * If internal applicants apply via our internal job board, the yellow “internal applicant” tag will automatically show in the GH profile. If you don’t see this tag, you can add it by navigating to the “Details” tab in GH and scroll down to “Source & Responsibility”. Click the pencil next to Source and select Internal Applicant from the drop down.
  * CES will use the internal applicant’s Google Calendar to find an appropriate time for them to meet, and do not need to be sent an availability email.
</details>

<details>
<summary markdown="span">Candidate Hygiene</summary>

  + <details><summary markdown="span">Merging candidates</summary>

    * Recruiters should [merge applicant profiles](https://support.greenhouse.io/hc/en-us/articles/115004506466-Merge-candidate-profiles) whenever you see the opportunity and can verify that the candidates are the same. Merging applicant profiles allows you to keep data up to date, and also ensures that you know the full application history of anyone who applies. Aside from Internal candidates who should be merged right before hiring, you can merge candidates at any stage of the process.
    * All of Talent Acquisition has access to merge profiles. You can merge candidate profiles by viewing the right hand toolbar on an applicant profile, and clicking on either the alert that appears at the top of the toolbar or ‘See More’ in the ‘Tools’ section.Before beginning, please ensure that the profiles are a match for each other by verifying that their emails, phone numbers, and/or resumes are the same.
    * When selecting which profile is Primary (right side), consider the following:
      * As a default, the most recent applicant profile should most likely be the Primary one, unless the most recent is a Prospect.
      * If this is an internal candidate, the most recent profile should always be Primary, but should not be merged until the candidate has accepted their offer to minimize the number of people with access to this information.
      * After merging, check the Activity Feed and Details tabs to see what information was removed from old profiles and ensure that you have the most accurate Recruiter, Coordinator, and Source.
      * If you have any questions, please contact the Enablement Manager because profile merges cannot be undone. 
    </details>

  + <details><summary markdown="span">Adding or transferring candidates between jobs</summary>

    * If a candidate is in one job and needs to move to another, it’s important to correctly determine whether they should be added or transferred. Both are accessible by clicking the ‘Add, Transfer, or Remove Candidate’s Jobs’ button on the bottom right corner of your candidate profile.
    * Before making any moves, always ensure the candidate does not have any interviews scheduled. Transferring will automatically remove scheduled interviews and make them invisible on the candidates’ Guide, but will not cancel the interview from the interviewers’ calendar or inform the candidate of what is happening. Always wait until the interview has been completed, or fully cancel the interview and reschedule under the new req.
    * When you add a candidate to a new job, the candidate starts the new job with a clean slate. There will be no scorecards or forms from any other position. This should be chosen when you are starting a brand new hiring process and will not be considering scorecards from any other job, and want their application date to be the date that you add them.
    * When you transfer a candidate from one job to another, most of their data before the reference check stage moves with them. Scorecards will be visible in the “Scorecards” tab and their original application date will remain. Scorecards that have not been submitted cannot be submitted after transfer, so make sure you collect those before you make your move. Offers and forms are also not transferable, so it’s important to transfer before a candidate hits the Reference Check stage. When you are moving someone from an Evergreen req into the role they will be hired into, you should always transfer. There will be no record of the previous job in any reporting.
    * Sometimes, deciding whether to add or transfer can be tricky. If a candidate interviewed for a role that was filled by someone else, and you get a new headcount a few months later, should you transfer them into the new job or add them and start over? One thing to consider is the implication your choice will have on the data. In this case, a candidate who transfers will appear to have been in the hiring process for months, when you’ve actually only been talking to them about this opening for a few days. At the same time, you want to save their scorecards because they’ll be relevant for the new job. In another situation, you could have an applicant who was being considered for the role, was rejected by the Hiring Manager, but contacted a few months later when the scope of the role changed and they were now considered a good fit. The key to knowing when to transfer is understanding what impact it would have on our data, and what solution would paint the most **accurate** picture. If you’re not sure whether to add or transfer, you should check in with Enablement to get another opinion.
    </details>
  
  + <details><summary markdown="span">Rejecting candidates</summary>
  
    * When rejecting candidates, it’s important to carefully review all rejection reasons and pick the one that most accurately describes why a candidate is being rejected. This is because data is analyzed on a quarterly basis by the Talent Acquisition Leadership team, People Leadership Team, and E-Group.
    * Once you’ve clicked the most detailed reason, include detailed context on the reason in the “Rejection Notes” box. Please be as specific as possible. For example, instead of writing “the candidate rejected because of compensation”, write “the candidate rejected because the salary and equity that we offered was much lower than a competing offer.”
    * Timing
      * Please note that you cannot submit scorecards after rejecting a candidate.
      * We will not cancel an interview within less than 24 hours' notice if the candidate is being rejected. The recruiter will notify the candidate that they are no longer in consideration for the role and present them with the option to continue with the scheduled interview slot. The interviewer must be informed, and will not discuss the feedback that led to the decline. The conversation will pivot to general opportunities at GitLab and answering any questions about the company and team that the candidate may have.
      * If the candidate's interview is outside 24 hours, the interview should be deleted in Greenhouse before the candidate is rejected and notified. The candidate should always be notified if they've been rejected, and the recruiter  is primarily responsible for declining the candidate. Any interviews scheduled will also not be automatically canceled, so you’ll need to ensure this happens first.
    </details>
  
</details>


## Other Hiring Pages

- [Greenhouse](/handbook/hiring/greenhouse/)
- [People Technology & Insights](/handbook/hiring/talent-acquisition-framework/talent-acquisition-operations-insights/)
- [Talent Acquisition Process Framework](/handbook/hiring/talent-acquisition-framework/)
- [Referral Operations](/handbook/hiring/referral-operations/)
- [Referral Process](/handbook/hiring/referral-process/)
- [Resource Guide](/handbook/hiring/guide/)
- [Sourcing](/handbook/hiring/sourcing/)



## Additional Resources

- [Overview of Job Families](/handbook/hiring/job-families)
- [Background checks](/handbook/people-policies/#background-checks)
- [Benefits](/handbook/total-rewards/benefits/)
- [Compensation](/handbook/total-rewards/compensation/)
- [Contracts](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/employment_contracts/)
- [GitLab talent ambassador](/handbook/hiring/gitlab-ambassadors/)
- [Onboarding](/handbook/people-group/general-onboarding)
- [Stock options](/handbook/stock-options)
- [Visas](/handbook/people-group/visas/)
